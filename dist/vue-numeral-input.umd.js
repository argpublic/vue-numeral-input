(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('numeral')) :
    typeof define === 'function' && define.amd ? define(['exports', 'numeral'], factory) :
    (global = global || self, factory(global.VueNumeralInput = {}, global.numeral));
}(this, (function (exports, numeral) { 'use strict';

    numeral = numeral && numeral.hasOwnProperty('default') ? numeral['default'] : numeral;

    //

    var script = {

        name: 'NumeralInput',

        props: {
            disabled: {},
            value: {}
        },

        mounted: function mounted() {
            this.formatValue();
        },
        watch: {
            value: function value(nVal, oldVal) {

                if (!nVal && nVal !== 0) {
                    this.$nextTick(function () {
                        $(this.$el).val('');
                    });

                    return;
                }

                var nValFormat = numeral(nVal).format('0.00');

                if (('' + nVal).slice(-1) != '.') {
                    this.$emit('input', numeral(nValFormat).value());
                }

                this.formatValue(false);
            }
        },
        methods: {
            selectAll: function selectAll(event) {
                // Workaround for Safari bug
                // http://stackoverflow.com/questions/1269722/selecting-text-on-focus-using-jquery-not-working-in-safari-and-chrome
                setTimeout(function () {
                    event.target.select();
                }, 0);
            },
            updateValue: function updateValue(value) {

                if (!value && value !== 0) {
                    this.$emit('input', '');

                    return;
                }

                var result = numeral(value).value();

                this.$emit('input', result);
            },
            formatValue: function formatValue() {
                var updateValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;


                var value = !this.value && this.value !== 0 ? '' : numeral(this.value).format('0,0.[00]');

                this.$nextTick(function () {

                    $(this.$el).val(value);

                    if (updateValue) {
                        this.updateValue(value);
                    }
                });
            }
        }
    };

    function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
    /* server only */
    , shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
      } // Vue.extend constructor export interop.


      var options = typeof script === 'function' ? script.options : script; // render functions

      if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true; // functional template

        if (isFunctionalTemplate) {
          options.functional = true;
        }
      } // scopedId


      if (scopeId) {
        options._scopeId = scopeId;
      }

      var hook;

      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context || // cached call
          this.$vnode && this.$vnode.ssrContext || // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true

          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          } // inject component styles


          if (style) {
            style.call(this, createInjectorSSR(context));
          } // register component module identifier for async chunk inference


          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        }; // used by ssr in case component is cached and beforeCreate
        // never gets called


        options._ssrRegister = hook;
      } else if (style) {
        hook = shadowMode ? function () {
          style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
        } : function (context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook) {
        if (options.functional) {
          // register for functional component in vue file
          var originalRender = options.render;

          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }

      return script;
    }

    var normalizeComponent_1 = normalizeComponent;

    /* script */
    var __vue_script__ = script;

    /* template */
    var __vue_render__ = function __vue_render__() {
      var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('input', { staticClass: "input form-control text-right", attrs: { "disabled": _vm.disabled }, domProps: { "value": _vm.value }, on: { "input": function input($event) {
            return _vm.updateValue($event.target.value);
          }, "blur": _vm.formatValue, "focus": function focus($event) {
            return _vm.selectAll($event);
          } } });
    };
    var __vue_staticRenderFns__ = [];

    /* style */
    var __vue_inject_styles__ = undefined;
    /* scoped */
    var __vue_scope_id__ = undefined;
    /* module identifier */
    var __vue_module_identifier__ = undefined;
    /* functional template */
    var __vue_is_functional_template__ = false;
    /* style inject */

    /* style inject SSR */

    var NumeralInput = normalizeComponent_1({ render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, undefined, undefined);

    // install function executed by Vue.use()
    function install(Vue) {
      if (install.installed) return;
      install.installed = true;
      Vue.component(NumeralInput.name, NumeralInput);
    }

    // Create module definition for Vue.use()
    var plugin = {
      install: install

      // To auto-install when vue is found
      /* global window global */
    };var GlobalVue = null;
    if (typeof window !== 'undefined') {
      GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
      GlobalVue = global.Vue;
    }
    if (GlobalVue) {
      GlobalVue.use(plugin);
    }

    // Inject install function into component - allows component
    // to be registered via Vue.use() as well as Vue.component()
    NumeralInput.install = install;

    // It's possible to expose named exports when writing components that can
    // also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
    // export const RollupDemoDirective = component;

    exports.default = NumeralInput;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
